class Person
  # def initialize (name, age) # CONSTRUCTOR
  #   @name = name      #   attr_accessor :name
  #   @age = age        #   attr_reader :age
  # end
  # def name  #Getter
  #   @name
  # end
  # def name= (new_name) # Setter
  #   @name = new_name
  # end

  attr_accessor :name, :age # getter and setter for name and age
end

# person1 = Person.new("Joe", 14)
# puts person1.name # Joe
# person1.name = "Mike" # Setter
#
# #puts person1.age # undefined method 'age' for #<Person

