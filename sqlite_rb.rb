#!/usr/bin/ruby

require "sqlite3"

class SqliteLocal
  def versionSQLite
    begin

      db = SQLite3::Database.new ":memory:"
      puts db.get_first_value 'SELECT SQLITE_VERSION()'

    rescue SQLite3::Exception => e

      puts "Exception occurred"
      puts e

    ensure
      db.close if db
    end
  end

  def insertData(artist, nameOfSong)
    begin

      db = SQLite3::Database.open "test.db"
      db.execute "CREATE TABLE IF NOT EXISTS SongsVK(Id INTEGER PRIMARY KEY AUTOINCREMENT, Artist TEXT, Name TEXT)"
      db.execute "INSERT INTO SongsVK (Artist, Name) VALUES('#{artist}','#{nameOfSong}')"

    rescue SQLite3::Exception => e

      puts "Exception occurred"
      puts e

    ensure
      db.close if db
    end
  end

  def fetchingData
    begin

      db = SQLite3::Database.open "test.db"

      stm = db.prepare "SELECT * FROM Cars LIMIT 5"
      rs = stm.execute

      rs.each do |row|
        puts row.join "\s"
      end

    rescue SQLite3::Exception => e

      puts "Exception occurred"
      puts e

    ensure
      stm.close if stm
      db.close if db
    end
  end
end