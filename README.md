##Weather forecast
Collecting data from forecast resource (using api). Processing data and provide results in chart/diagram or send email(?).



1. Collect data from forecast resource:	
	* investigate how to use API of google/yahoo/other resources;	
	* collect data and write to file(JSON/XML?);
2. Postgres and C#:	
	* investigate how to connect to postgres db (heroku);	
	* create tables, read/write data to db;	
	* create/design tables that will be used for storing data from forecast;
3. Forecast + Postgres:	
	* read data from forecast service and write to database;	
	* read data from forecast and output result to file;
4. adp.net webipa + java script
5. Create table with result of weather forecast (data should be collected from db)
6. Create charts/diagrams in view of forecast data at db;
7. log4net - additional page for managing application logs
8. load testing (Jmeter+NewRelic?)

** Docker + Linux/OSx

**Toolset/Technology**: MS Visual Studio, .net; postgres db(heroku), postgres client, git repo (bitbucket), git client