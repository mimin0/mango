class ParsJson
  require 'json'

  def readFile
    file = File.read('zips.json')
    data_hash = JSON.parse(file)
    return data_hash
  end

  def city
    return readFile["title"]
  end

end