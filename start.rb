# # 4.times {puts 'Hello Wold'
# # }
# #
# # if Integer === 21
# #   puts 'true'
# # end
#
#
#
# ### -- Handling exception --
# begin
#
#   File.foreach('zips1.json') do |line|
#     puts line
#   end
#
# rescue Exception => e
#   puts e.message
#   puts "let's pretend this didnt happend..."
# end
#
#
# ### -- alternative to Exceptions
# if File.exist? 'zips2.json'
#   File.foreach('zips.json') do |line|
#     puts line
#   end
# end
#
#
# ### -- srings / interpolation
# single_quoted = 'ice cream \n followed by it\'s party'
# double_quotes = "ice cream \n followed by it\'s party"
# puts single_quoted
# puts double_quotes
#
# def multiply (one, two)
#   "#{one} multiplied by #{two} equals #{one * two}"
# end
# puts multiply(5,3)
#
#
# ### -- More strings
# cur_weather = %Q{It's a hot day outside
#                 Grab your umbrallas...}
# cur_weather.lines do |line|
#   line.sub! 'hot', 'rainy' # substitute 'hot' with 'rainy'
#   puts "#{line.strip}"
# end
#
#
# ### -- Arrays
# het_arr = [1, "two", :three] #heterogeneous types
# puts het_arr[1] # => two (array indices start at 0)
#
# arr_words = %w{ what a great day today! }
# puts arr_words[-2] # => day
# puts "#{arr_words.first} - #{arr_words.last}" # => what - today!
# p arr_words[-3,2] # => ["great", "day"] (go back 3 and take 2 elements)
#
# #(Range type covered later...)
# p arr_words[2..4] # => ["great", "day", "today"]
#
# # Make a String out of array elements separated by ','
# puts arr_words.join(',') # => what,a,great,day,today
#
#
# ### -- Rages
# p ('k'..'z').to_a.sample(2) # => ["k","z"]



#require File.dirname(__FILE__) + '/person'

# require_relative 'person'
#
# person1 = Person.new
# p person1.name
# person1.name = 'Mike'
# puts person1.name


require_relative ('Pars_Json')
fileJ = ParsJson.new
puts fileJ.city
